# Vocal Command Measure
## Table of Contents
1. [Overall description](#overall-description)
3. [Requirements](#requirements)
4. [Installation](#installation)
5. [How to use](#how-to-use)
6. [How to customize](#how-to-customize)
7. [Licence](#licence)

## Overall description
This packages is the same as the vocal command packages but will evaluate the system and create a excel document to store this evaluation. This evaluation can than be used in a google sheet document to get WER and RTF value and overall execution time.

A dedicated executable let you compare wake up words performence.

last results for overall measurement are [here](https://docs.google.com/spreadsheets/d/e/2PACX-1vT-AAAIVON5THEgS90nG9Rt_zy5O1v81awueySt63okzqWU_TUvuj3LbCPpx0D0L0R6cO3v9gxGFfQV/pubhtml?widget=true&amp;headers=false)

last result for wake up word measurement are [here](https://docs.google.com/spreadsheets/d/e/2PACX-1vSJS2pFCUtlGf173DuqYzlKou21S6ebV47TCNuWOG_oUJmrSBoc0w6X-9ry6PHUe4Ky3A8RPDnLyNnv/pubhtml?widget=true&amp;headers=false)

You can use the Apps script file of this git to get easily readable results.

## Requirements
They are the same as the vocal command packages but you will need openxls python library in addition to the other library.

## Installation
It is the same as the vocal command packages but you will need the copy and past the measure packages instead.

## How to use

source ROS :

- `source /opt/ros/humble/setup.bash`

source workspace :

- go to workspace root
- `source install/local_setup.bash`

for overall measurement :

- launch the launch file :

    - `ros2 launch vocal_command_measure vocal_command_measure_launch.py`

- when everything is launched :

    - juste wait for the programm to run. You can kill the programm when the screen print "END END END END" in red

for wake up word comparison:

- execute the ww_test script :

    - `ros2 run vocal_command_measure ww-test`.

- when everything is running :

    - juste wait for the end of the program.

## How to customize
for overall measurement :
- add more audio :
    - add an audio command in the test_for_measure repository. The name must be a number following the last audio file.
    - add it's name and content in the two dictionnary in the global variables of voice_publisher.
- add more round :
    - change the variable "number_round" in the global variables of voice_publisher.

for wake up word comparison ;
- add more audio :
    - add an audio containing the wake up word you want to compare to the test_for_ww repository. The name must be a number following the last audio file.
    - add it's name and content in the two dictionnary in the global variables of the ww_test file

## Licence
Project developped by Lucie JEANNIN with Alexis BIVER and Raphael LARTOT for Inria Nancy.
