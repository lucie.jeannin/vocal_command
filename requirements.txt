faster-whisper==0.6.0
pyAudio
SpeechRecognition==3.10.0
torch==2.0.1
word2number==1.1
pydub==0.25.1
