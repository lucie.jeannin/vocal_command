# Vocal Command
## Table of Contents
- [Vocal Command](#vocal-command)
  - [Table of Contents](#table-of-contents)
  - [Overall description](#overall-description)
  - [Files description](#files-description)
  - [Use with docker](#use-with-docker)
    - [Tiago simulation](#tiago-simulation)
  - [Installation From source](#installation-from-source)
    - [Requirements](#requirements)
    - [Installation steps](#installation-steps)
  - [How to use](#how-to-use)
  - [List of possible command](#list-of-possible-command)
  - [Troubleshooting](#troubleshooting)
  - [How to customize](#how-to-customize)
  - [Models](#models)
  - [How to add recognized command](#how-to-add-recognized-command)
  - [Licence](#licence)

## Overall description
This repository contains a library for a vocal command that allows you to move a robot.
This program receives live audio input and transforms it into a command line that controls the movements of a mobile robot.
It also have a wake up word detection that you can easily modify.

It works best with a unidirectional microphone.

![illustration](https://lh3.googleusercontent.com/drive-viewer/AFGJ81p09KICuwMD-e4ONGJCzi_80KaXOfrmcypR65vImwyWb7rNEIXcbC1mQnUc6JNZ-JsWOEAULYlleswxdp7Yebjx3A8tOQ=s1600)

## Files description
vocal_command packages :
* voice_publisher
    *  get the audio buffer and publish it constantly.
* ww_publisher
    * use audio buffer to find wake up word or "stop" and publish it when found.
* asr_publisher
    * transcribe the audio buffer and publish it but only when wake up word was published.
* txt_to_comd_pubsub
    * wait for audio re transcription and transform it into command for the robot and publish it.
* exec_subscriber
    * wait for command and send it to the robot.

* safety
    * make sure every node is publishing, if one or multiple node stops publishing for a certain time this noce will stop the robot and tell the user that there is an issue

* sound
    * make the sound to indicate the progress of the comprehension of the nodes.
* configuration
    * configuration file that can be modified.

vocal_command_measure packages :
- the files are the same but the voice_publisher file will use pre recorded audio file to calculate the execution time and the WER and RTF value. (word error rate and real time factor)

## Use with docker
You can use the Dockerfile to build a docker image for the repository  
Don't forget to update the config/configuration.ini file depending on your environment, and rebuild the image accordingly.  
To give the docker container access to your microphone and speakers using `--devices` and/or `--volume` args for your `docker run` command  
You can build the image and running it in a container using the following:  
`docker build . -t vocal_command:latest`  
`docker run -it --name vocal_command --device /dev/snd:/dev/snd --net host -v/dev/shm:/dev/shm --privileged vocal_command`  
Once in the docker environment you can use the command to start the ros2 nodes:
`ros2 launch vocal_command vocal_command_launch.py`  
You can see examples of such commands in the `docker-compose.yml` file (which role is described below).  

### Tiago simulation

The `docker-compose.yml` at the root of this repo shows the use of this programm to control a tiago in gazebo.  
It prepares the following containers:  
- roscore: a simple container from image `ros:noetic-ros-core` that will serve as our `ros master`
- ros_bridge: container with ros1 and ros2 that runs `ros2 run ros1_bridge dynamic_bridge`. Based on the image `registry.gitlab.inria.fr/eurobin-horizon/dockers/` (please check you have the access to this repository)
- tiago_sim: Docker image from the (ros.org tiago tutorial)[http://wiki.ros.org/Robots/TIAGo/Tutorials]. Please refer to this documentation if you encounter an issue with this container.
- ros2_vocal_command: The Dockerimage from this repo running the ros2 nodes.

## Installation From source

### Requirements
- python3.10
- ROS2 humble
- speech recognition
- faster whisper
- pyaudio
- torch
- word2number
- sox
- configparser
- pydub

See complete list in the requirements file
Use : `pip install -r requirements.txt`

ROS2 should be installed following the
[ROS2 installation guide](https://docs.ros.org/en/humble/Installation.html)

### Installation steps

1. create virtual environment

    * `python3 -m venv /path/to/new/virtual/environment`

2. source virtual environment

    * `source /path/to/new/virtual/environment/bin/activate`

3. clone git

    * `git clone https://gitlab.inria.fr/lucie.jeannin/vocal_command.git`

4. install requirements

    * `cd vocal_command`

    * `pip install -r requirements.txt`

    * `sudo apt-get install sox`

    * _make sur ROS2 is also installed in this environment_

5. create a workspace

    * `mkdir -p ~/ros2_ws/src`

6. copy and past the vocal_command file into the src file. The overall worplace should look like this :

    * ros2_ws
        * src
            * vocal_command
                * package.xlm
                * CMakeLists.txt
                * setup.py
                * setup.cfg
                * launch
                    * vocal_command_launch.py
                * config
                    * configuration.ini
                * sound
                    * end_listening.wav
                    * not_understood.wav
                    * partially_understood.wav
                    * start.wav
                    * stop.wav
                    * understood.wav
                    * WuW.wav
                * vocal_command
                    * __ init __.py
                    * asr_publisher.py
                    * exec_subscriber.py
                    * safety.py
                    * sound.py
                    * txt_to_comd_pubsub.py
                    * voice_publisher.py
                    * ww_publisher.py
                * ressource
                    * vocal_command
                * test
                    * test_copyright.py
                    * test_flake8.py
                    * test_pep257.py


7. return at the root of the workspace and build the package
    * `cd ..`
    * `rosdep install -i --from-path src --rosdistro humble -y`
    * `colcon build`

## How to use
source ROS :
* `source /opt/ros/humble/setup.bash`

source workspace :
* go to workspace root
* `source install/local_setup.bash`

launch the launch file :
* `ros2 launch vocal_command vocal_command_launch.py`

When everything is launched :
* say wake up word : "hello you".
* when hear confirmation say command.
* everything else will be automated.
* after each command you need to say the wake up word again.
* you can say stop to stop the robot any time even without having said the wake up word.

* __when using it for the first time, the program will need to download the model you are using. This will take some time but will only be done once.__

![demonstration](https://drive.google.com/uc?export=download&id=172b7vvkwSdfUJf2kG803GZG5XAiaOLT_)

## List of possible command
* go left/right/back/backward (say anything else after go it will go forward)
* turn left/right
* go for [give time is second or minute / distance in meter or centimeter]
* turn for [give degree]
* change/set speed to
* go slower/faster
* go a little/bit/lot/much more/further


## Troubleshooting
* ERROR : chosen device not available
    * close all application that use the mic. Even parameters.
    * to find other device name you could use : `arecord -l` and put the name in the brackets in the chosen_mic variable
* ModuleNotFoundError: No module named 'pyaudio'
    * reinstall pyaudio with : `sudo apt-get install python3-pyaudio` or `pip install pyaudio`
* ValueError: could not convert string to float in exec_subscriber file
    * make sur the default speed in txt_to_comd_pubsub file is of form "a.b" and note a (example : 3.0 and not 3)
* ERROR: the following rosdeps failed to install pip: Failed to detect successful installation of [torchvision]
    * ignore this error if the launch file run successfully
* ModuleNotFoundError: No module named [any already installed module]
    * add those two line at the very beggining of the ww_publisher and the asr_publisher files :
        `import sys`
        `sys.path.append('path/to/your/venv/lib/python<your_python_version>/site-packages')`
* RuntimeError : unable to open file 'model.bin'
    * choose another model or you can downloads your model.bin file at [Hugging Face Guillaume klein page](https://huggingface.co/guillaumekln) on clicking on the model you want and then 'files and versions'.

        __Note that the file might be to big for your computer to actually downloads.__
* NumbaDeprecationWarning
    * ignore this warning
* ALSA lib warning : unknown PCM card / found no matching channel / cannont open device / invalid field card / invalid card
    * ignore this warning
* FileNotFoundError: [Errno 2] No such file or directory: 'ffmpeg'
    * `sudo apt install ffmpeg`
* keyError : 
    * change configuration file path (path_to_file) into your absolute path to the file in all the files 

If you are using python 3.8 you may encounter the following error :
* ImportError: cannot import name 'TypeAlias' from 'typing_extensions'
    * run : `pip uninstall typing_extensions``pip install typing_extensions==4.0.0`
* RuntimeError: module compiled against API version a but this version of numpy is 9
    * run : `pip install numpy --upgrade`

## How to customize
- change the microphone in the configuration file by changing it's name sample rate and sample width in the source category.
- change the words recognize when waiting for the wake up word in the configuration file in the wake.word category.
- change the models in the model category of the configuration file.
- change the default speed and distance to go when we need to go a "bit" and a "lot" further in the command.building category in the configuration file.
- change the max time a command is executed without beeing stoped in the safety category of the configuration file.
- change all three time limites that help find an unresponsive node in the safety category of the configuration file.
- change the variables defining a silence in the silence category of the configuration file.
- change voice_publisher global variables to change the way the audio is captured.
- change the robot your are moving by changing the topic name in the exec_subscriber file.

- __Don't forget to use `colcon build` at the root of your workspace after each modification.__

## Models
List of models you can use :
* tiny.en
* base.en
* small.en
* medium.en

These models are English only and work better. But other models do exist but doesn't interest us, since only English commands are recognized by the programme.

Bigger the model better the result but slowest the program.

## How to add recognized command
1. build the command
    * go to the txt_to_com_pubsub file
    * add the vocabulary you will use in the command list vocab
    * go to the build command function
    * add if statement for the vocabulary that should be recognise
    * under the new if statement build the command or the piece of command that you need to execute what you want
2. execute the command
    * go to exec_subscriber file
    * go to Pubsub_move class
    * go to listener_callback function
    * make sur your command will be working with the already existing if statement

    or

    * add if statement that will recognize only your command line
    * publish the right line to execute your command


## Licence
Project developped by [Lucie JEANNIN](luciejeannin77@gmail.com) with Alexis BIVER and Raphael LARTOT for Inria Nancy.
