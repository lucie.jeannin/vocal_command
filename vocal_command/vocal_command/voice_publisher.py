import configparser
import io
import pyaudio
import rclpy
import speech_recognition as sr
import wave

from datetime import datetime
from pathlib import Path
from pydub import AudioSegment, silence
from queue import Queue
from rclpy.node import Node
from std_msgs.msg import ByteMultiArray, String
from tempfile import NamedTemporaryFile

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -configuration variables-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

config = configparser.ConfigParser()

# Get right path
path_to_workspace_install = Path(__file__).parent.parent.parent.parent.parent.resolve()
path_to_config = path_to_workspace_install / "share" / "vocal_command" / "config" / "configuration.ini"

# Use new found path
config.read(str(path_to_config))

CHOSEN_MIC = config["source"]["mic_name"]
SAMPLE_RATE = int(config["source"]["SampleRate"])
SAMPLE_WIDTH = int(config["source"]["SampleWidth"])

MAX_SILENCE = int(config["silence"]["MaxSilenceLen"])
SILENCE_TRESH = int(config["silence"]["SilenceTresh"])

TIME_LAPSE = float(config["safety"]["TimeLapse"])

# -globale variables ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Thread safe Queue for passing data from the threaded recording callback
data_queue = Queue()

# We use SpeechRecognizer to record our audio because it has a nice feauture where it can detect when speech ends.
recorder = sr.Recognizer()

temp_file = NamedTemporaryFile().name

# -editable variables -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

recorder.energy_threshold = 1000  # volume minimum at which the recorder start recording
# Definitely do this, dynamic energy compensation lowers the energy threshold dramtically to a point where the SpeechRecognizer never stops recording.
recorder.dynamic_energy_threshold = False

record_timeout = 2

# -communication-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

class Publisher_audio(Node):
    """publish audio buffer
    """
    def __init__(self):
        """initialise the publisher
        """
        super().__init__('publisher_audio')
        self.started = ""

        self.get_logger().info("\033[0;95m\nStarting nodes\033[0m")

        self.publisher_audio = self.create_publisher(ByteMultiArray, '/vocal_command/audio_buffer', 10)

        self.subscriber_safety = self.create_subscription(String, '/vocal_command/safety', self.listener_safety, 10)
        self.subscriber_safety  # prevent unused variable warning

        self.publisher_safety = self.create_publisher(String, '/vocal_command/safety', 10)
        timer_period = TIME_LAPSE  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback_safety)

    def timer_callback_safety(self):
        """action to do to notice the safety node that this node is functionning
        """
        msg = String()
        msg.data = "1"
        self.publisher_safety.publish(msg)

    def listener_safety(self, msg):
        if msg.data == "12345":
            self.started = "12345"
            self.get_logger().info("\033[0;95m\nListening\033[0m")

    def find_source(self, mic):
        """Trouver le nom du micro

        Args:
            mic (pyaudio): pyaudio microphone

        Returns:
            string tuple: id of the microphone we want and its name used for the sox play command line
        """
        for idx in range(mic.get_device_count()):
            info = mic.get_device_info_by_index(idx)
            if CHOSEN_MIC in info["name"]:
                return idx
        self.get_logger().info("\033[0;91mERROR: chosen device not available\033[0m")


def main():
    """main function to launch the publisher
    """
    rclpy.init()

    global data_queue

    # -initialization------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # Important for linux users.
    # Prevents permanent application hang and crash by using the wrong Microphone
    publisher = Publisher_audio()
    index = publisher.find_source(pyaudio.PyAudio())
    source = sr.Microphone(sample_rate=SAMPLE_RATE, device_index=index)

    try:
        with source:
            recorder.adjust_for_ambient_noise(source)
    except AttributeError:
        publisher.get_logger().info("The chosen audio device is already beeing used in another application")

    # Create a background thread that will pass us raw audio bytes.
    # We could do this manually but SpeechRecognizer provides a nice helper.
    recorder.listen_in_background(source, record_callback, phrase_time_limit=record_timeout)

    # ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    last_sample = bytes()


    while True:
        if publisher.started == "12345":
            # Pull raw recorded audio from the queue.
            if not data_queue.empty():
                phrase_complete = False

                # Concatenate our current audio data with the latest audio data.
                while not data_queue.empty():
                    data = data_queue.get()
                    last_sample += data

                # If enough time has passed between recordings, consider the phrase complete.
                # Clear the current working audio buffer to start over with the new data.
                if is_silence(last_sample):
                    phrase_complete = True

                if phrase_complete:
                    try :
                        msg = ByteMultiArray()
                        msg.data = devide(last_sample)
                        publisher.publisher_audio.publish(msg)
                        publisher.get_logger().debug('Publishing audio buffer')
                        last_sample = bytes()
                    except AssertionError:
                        pass

        rclpy.spin_once(publisher)


# -functions---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def devide(sample):
    """devide the sample into a table of unique byte

    Args:
        sample (Bytes): the sample that needs to be devided, here an audio sample

    Returns:
        table of bytes: a table of unique byte composed of each bytes of the sample
    """
    devided_sample = []
    for i in sample:
        devided_sample.append(bytes([i]))
    return devided_sample


def is_silence(data):

        # Use AudioData to convert the raw data to wav data.
        sample = data

        # # Use AudioData to convert the raw data to wav data.
        audio_data_sample = sr.AudioData(sample, SAMPLE_RATE, SAMPLE_WIDTH)
        wav_data_sample = io.BytesIO(audio_data_sample.get_wav_data())

        # Write wav data to the temporary file as bytes.
        with open(temp_file, 'w+b') as f:
            f.write(wav_data_sample.read())

        myaudio = AudioSegment.from_wav(temp_file)
        dBFS = myaudio.dBFS
        detected_silence = silence.detect_silence(myaudio, min_silence_len = MAX_SILENCE, silence_thresh = dBFS - SILENCE_TRESH)

        if len(detected_silence) != 0:
            with wave.open(temp_file) as mywav:
                audio_duration = mywav.getnframes() / mywav.getframerate()

            stop = detected_silence[len(detected_silence)-1][1]

            # make sure the silence is located at the end of the audio temp_file
            if (audio_duration - stop < 1) :
                return True
            else:
                return False
        else:
            return False


def record_callback(_, audio: sr.AudioData) -> None:
    """Threaded callback function to recieve audio data when recordings finish.
    audio: An AudioData containing the recorded bytes.
    """
    # Grab the raw bytes and push it into the thread safe queue.
    data = audio.get_raw_data()
    data_queue.put(data)


# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
