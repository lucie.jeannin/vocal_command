import configparser
import rclpy

from pathlib import Path
from rclpy.node import Node
from std_msgs.msg import String
from time import time

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -configuration variables-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

config = configparser.ConfigParser()


# Get right path
path_to_workspace_install = Path(__file__).parent.parent.parent.parent.parent.resolve()
path_to_config = path_to_workspace_install / "share" / "vocal_command" / "config" / "configuration.ini"

# Use new found path
config.read(str(path_to_config))

TIME_LIMITE = float(config["safety"]["TimeLimite"])
TIMELIMITE_INIT = float(config["safety"]["TimeLimiteInit"])

# -Communication-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

class Safety_sub(Node):
    """make sure all nodes are still active
    """
    def __init__(self):
        """initialiaze subscriber publisher
        """
        super().__init__('Safety_sub')

        self.t01 = time()
        self.t02 = time()
        self.t03 = time()
        self.t04 = time()
        self.t05 = time()

        self.started_node = ""
        self.init_delta = 0
        self.begining = time()
        self.once = True

        self.subscriber = self.create_subscription(String, '/vocal_command/safety', self.listener_callback, 10)
        self.subscriber  # prevent unused variable warning

        self.publisher = self.create_publisher(String, '/vocal_command/safety', 10)

        self.publisher_stop = self.create_publisher(String, '/vocal_command/stop', 10)

    def listener_callback(self, msg):
        """action to do when we hear a message from any other node

        Args:
            msg (string): id of the node
        """
        delta={}

        if self.started_node == "12345":
            if msg.data == "1":
                self.t01 = time()
            if msg.data == "2":
                self.t02 = time()
            if msg.data == "3":
                self.t03 = time()
            if msg.data == "4":
                self.t04 = time()
            if msg.data == "5":
                self.t05 = time()

            delta1 = time() - self.t01
            if delta1 > TIME_LIMITE:
                delta["1 "] = delta1

            delta2 = time() - self.t02
            if delta2 > TIME_LIMITE:
                delta["2 "] = delta2

            delta3 = time() - self.t03
            if delta3 > TIME_LIMITE:
                delta["3 "] = delta3

            delta4 = time() - self.t04
            if delta4 > TIME_LIMITE:
                delta["4 "] = delta4

            delta5 = time() - self.t05
            if delta5 > TIME_LIMITE:
                delta["5 "] = delta5


            if delta != {} and self.init_delta > TIMELIMITE_INIT:

                if self.once:
                    self.get_logger().info("\033[0;91mSTOP\033[0m")
                    if len("".join(delta.keys())) > 2:
                        self.get_logger().info('\033[0;91mMULTIPLE NODES STOPPED: they are "%s"\033[0m' % ("".join(delta.keys())))
                    else :
                        self.get_logger().info('\033[0;91mONE NODE STOPPED: it is "%s"\033[0m' % ("".join(delta.keys())))
                    self.once = False
                    msg = String()
                    msg.data = "stop"
                    self.publisher_stop.publish(msg)

            elif delta != {} and self.init_delta < TIMELIMITE_INIT:
                self.begining = time()

            elif delta == {} and self.init_delta < TIMELIMITE_INIT:
                self.init_delta = self.begining - self.init_delta
        else:
            if msg.data == "1" and "1" not in self.started_node:
                self.started_node += "1"
            if msg.data == "2" and "2" not in self.started_node:
                self.started_node += "2"
            if msg.data == "3" and "3" not in self.started_node:
                self.started_node += "3"
            if msg.data == "4" and "4" not in self.started_node:
                self.started_node += "4"
            if msg.data == "5" and "5" not in self.started_node:
                self.started_node += "5"

            if "1" in self.started_node and "2" in self.started_node and "3" in self.started_node and "4" in self.started_node and "5" in self.started_node:
                self.started_node = "12345"
                ms = String()
                ms.data = "12345"
                self.get_logger().info('publishing: "%s"' % (ms.data))
                self.publisher.publish(ms)


def main():
    """main function to launch the publisher and subscriber
    """
    rclpy.init()
    pubsub = Safety_sub()
    rclpy.spin(pubsub)


# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
