import configparser
import io
import rclpy
import speech_recognition as sr
import string


from pathlib import Path
from rclpy.node import Node
from std_msgs.msg import ByteMultiArray, String
from tempfile import NamedTemporaryFile
from faster_whisper import WhisperModel

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -configuration variables-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

config = configparser.ConfigParser()

# Get right path
path_to_workspace_install = Path(__file__).parent.parent.parent.parent.parent.resolve()
path_to_config = path_to_workspace_install / "share" / "vocal_command" / "config" / "configuration.ini"

# Use new found path
config.read(str(path_to_config))

SAMPLE_RATE = int(config["source"]["SampleRate"])
SAMPLE_WIDTH = int(config["source"]["SampleWidth"])

WAKEUPWORD = config["wake.word"]["WakeUpWord"]
MODEL = config["model"]["ModelAsr"]

TIME_LAPSE = float(config["safety"]["TimeLapse"])

# -globale variables ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Load / Download model
audio_model = WhisperModel(MODEL)

temp_file = NamedTemporaryFile().name

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -communication-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

class PubSubTxt(Node):
    """publishing audio retranscritpion and subscribing to the wake up word, the exec topic to get key words when to restart and audio_buffer to get the audio
    """

    def __init__(self):
        """initialize subscribers and publisher
        """
        super().__init__('PubSub_txt')

        self.audio = []  # audio buffer
        self.ww = ""  # wake up word

        self.publisher = self.create_publisher(String, '/vocal_command/text', 10)
        self.publisher_sound = self.create_publisher(String, '/vocal_command/sound', 10)

        self.subscription_wakeup = self.create_subscription(String, '/vocal_command/wakeup', self.listener_callback, 10)
        self.subscription_wakeup  # prevent unused variable warning

        self.subscription_exec = self.create_subscription(String, '/vocal_command/exec', self.listener_callback_exec, 10)
        self.subscription_exec  # prevent unused variable warning

        self.subscription_audio = self.create_subscription(ByteMultiArray, '/vocal_command/audio_buffer', self.listener_callback_audio, 10)
        self.subscription_audio  # prevent unused variable warning

        self.publisher_safety = self.create_publisher(String, '/vocal_command/safety', 10)
        timer_period = TIME_LAPSE  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback_safety)

    def timer_callback_safety(self):
        """action to do to notice the safety node that this node is functionning
        """
        msg = String()
        msg.data = "3"
        self.publisher_safety.publish(msg)

    def listener_callback_audio(self, msg):
        """action to do when we hear an audio buffer

        Args:
            msg (table of bytes): audio that needs to be transcribe
        """
        if self.ww != "":
            self.get_logger().debug('I heard an audio buffer')
            self.audio = msg.data
            msg = String()
            try:
                msg.data = audio_to_text(self.audio)
                self.publisher.publish(msg)
                self.get_logger().info('Publishing: "%s"' % msg.data)
                self.ww = ""
                self.audio = []
            except AssertionError:
                self.get_logger().info("\033[0;95m\nWe didn't hear you\033[0m")
                self.audio = []

    def listener_callback(self, msg):
        """action to do when we hear the wake up word

        Args:
            msg (string): wake up word
        """
        self.ww = msg.data
        self.get_logger().debug('I heard: "%s"' % self.ww)
        msg_sound = String()
        msg_sound.data = "WuW"
        self.publisher_sound.publish(msg_sound)
        self.get_logger().debug('Publishing: "%s" sound' % msg_sound.data)

    def listener_callback_exec(self, msg):
        """action to do when we hear the key word "partially_understood"

        Args:
            msg (string): key words or command, only "partially_understood" will not be ignored
        """
        if msg.data == "partially_understood":
            self.get_logger().debug('I heard: "%s"' % msg.data)
            self.get_logger().info("\033[0;95m\nWe missed some information, could you repeat ?\033[0m")
            msg_sound = String()
            msg_sound.data = "partially_understood"
            self.publisher_sound.publish(msg_sound)
            self.get_logger().debug('Publishing: "%s" sound' % msg_sound.data)
            self.ww = WAKEUPWORD


def main():
    """main function to launch the publisher and subscriber
    """
    try:
        rclpy.init()
    except RuntimeError:
        pass

    pubsub = PubSubTxt()
    rclpy.spin(pubsub)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    # pubsub.destroy_node()
    # rclpy.shutdown()


# -function----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def audio_to_text(data):
    global audio_model
    global temp_file

    # Current raw audio bytes.
    last_sample = b''.join(data)

    # Use AudioData to convert the raw data to wav data.
    audio_data = sr.AudioData(last_sample, SAMPLE_RATE, SAMPLE_WIDTH)
    wav_data = io.BytesIO(audio_data.get_wav_data())

    # Write wav data to the temporary file as bytes.
    with open(temp_file, 'w+b') as f:
        f.write(wav_data.read())

    # Read the transcription.
    result = audio_model.transcribe(temp_file, vad_filter=True)

    try:
        for segment in result[0]:
            line = segment.text

        if line[-1:] in string.punctuation:
            return line[:-1].lower()
        else:
            return line.lower()
    except UnboundLocalError:
        pass


# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
