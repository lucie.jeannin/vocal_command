import configparser
import os
import pyaudio
import rclpy

from pathlib import Path
from rclpy.node import Node
from std_msgs.msg import String

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -configuration variables-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

config = configparser.ConfigParser()


# Get right path
path_to_workspace_install = Path(__file__).parent.parent.parent.parent.parent.resolve()
path_to_config = path_to_workspace_install / "share" / "vocal_command" / "config" / "configuration.ini"

# Use new found path
config.read(str(path_to_config))

CHOSEN_MIC = config["source"]["speaker_name"]

# -global variables-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

path_to_sound = str(path_to_workspace_install / "share" / "vocal_command" / "sound")

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -communication-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

class Sub(Node):
    """node that play the wanted sound
    """
    def __init__(self):
        """initialiaze subscriber
        """
        super().__init__('Sub')

        self.subscriber = self.create_subscription(String, '/vocal_command/sound',  self.listener_callback, 10)
        self.subscriber  # prevent unused variable warning
        self.device_name = ""

        # We find available devices
        self.mic = pyaudio.PyAudio()
        try:
            self.device_name = self.find_source()
        except TypeError:
            self.get_logger().info("the device in not available. Close all applications that use the chosen mic")

    def find_source(self):
        """find mic name
        """
        for idx in range(self.mic.get_device_count()):
            info = self.mic.get_device_info_by_index(idx)
            if CHOSEN_MIC in info["name"]:
                if info["name"]!="sysdefault":
                    return info["name"][-7:-1]
                else:
                     return info["name"]
        self.get_logger().info("\033[0;91mERROR: chosen device not available\033[0m")

    def listener_callback(self, msg):
        try :
            os.system("export AUDIODEV=" + self.device_name + "&&" + "export AUDIODRIVER=alsa" + "&&" + "play " + path_to_sound + "/" + msg.data + ".wav -q")
        except TypeError:
            self.get_logger().info("\033[0;91mERROR: chosen device not available\033[0m")


def main():
    """main function to launch the publisher and subscriber
    """
    try:
        rclpy.init()
    except RuntimeError:
        pass

    sub = Sub()
    rclpy.spin(sub)


# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
