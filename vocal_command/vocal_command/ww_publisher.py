import configparser
import io
import rclpy
import speech_recognition as sr
import string

from pathlib import Path
from rclpy.node import Node
from std_msgs.msg import ByteMultiArray, String
from tempfile import NamedTemporaryFile
from faster_whisper import WhisperModel

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -configuration variables-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

config = configparser.ConfigParser()


# Get right path
path_to_workspace_install = Path(__file__).parent.parent.parent.parent.parent.resolve()
path_to_config = path_to_workspace_install / "share" / "vocal_command" / "config" / "configuration.ini"

# Use new found path
config.read(str(path_to_config))

SAMPLE_RATE = int(config["source"]["SampleRate"])
SAMPLE_WIDTH = int(config["source"]["SampleWidth"])

WAKEUPWORD = config["wake.word"]["WakeUpWord"]
STOP = config["wake.word"]["StopWord"]

MODEL = config["model"]["ModelWuw"]

TIME_LAPSE = float(config["safety"]["TimeLapse"])

# -globale variables ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Load / Download model
audio_model = WhisperModel(MODEL)

temp_file = NamedTemporaryFile().name

# -communication-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

class PublisherSubscriberWW(Node):
    """publish wake up word and subscribe to exec to listen to key words to know when to restart
    """
    def __init__(self):
        """initialise the publisher and subscriber
        """
        self.start_listening = True
        super().__init__('publisher_subscriber_ww')
        self.publisher_wakeup = self.create_publisher(String, '/vocal_command/wakeup', 10)

        self.publisher_stop = self.create_publisher(String, '/vocal_command/stop', 10)

        self.subscription_exec = self.create_subscription(String, '/vocal_command/exec', self.listener_callback, 10)
        self.subscription_exec  # prevent unused variable warning

        self.subscription_audio = self.create_subscription(ByteMultiArray, '/vocal_command/audio_buffer', self.listener_callback_audio, 10)
        self.subscription_audio  # prevent unused variable warning

        self.publisher_safety = self.create_publisher(String, '/vocal_command/safety', 10)
        timer_period = TIME_LAPSE  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback_safety)

    def timer_callback_safety(self):
        """action to do to notice the safety node that this node is functionning
        """
        msg = String()
        msg.data = "2"
        self.publisher_safety.publish(msg)

    def listener_callback(self, msg):
        """action to do to listen to the key words to know when to restart

        Args:
            msg (string): key words 'not_understood" or "ok" or other key words and commands that will be ignored here
        """
        if msg.data == "not_understood" or msg.data == "ok":
            self.get_logger().debug('I heard: "%s"' % msg.data)
            self.start_listening = True

    def listener_callback_audio(self, msg):
        """action to do when we hear an audio buffer

        Args:
            msg (table of bytes): audio that needs to be transcribe
        """
        self.get_logger().debug('I heard an audio buffer')
        wake_up_word = WuW(msg.data)
        if wake_up_word is not None:
            msg = String()
            if self.start_listening or wake_up_word == STOP:

                if wake_up_word == WAKEUPWORD:
                    msg.data = WAKEUPWORD
                    self.publisher_wakeup.publish(msg)
                    self.start_listening = False
                elif wake_up_word == "stop":
                    msg.data = "stop"
                    self.publisher_stop.publish(msg)
                    self.start_listening = False

                self.get_logger().info('Publishing: "%s"' % msg.data)
        else:
            self.get_logger().info("\033[0;95m\nWe didn't hear you\033[0m")


def main():
    """main function to launch the subscribers
    """
    try:
        rclpy.init()
    except RuntimeError:
        pass

    node = PublisherSubscriberWW()

    rclpy.spin(node)
    # node.destroy_node()
    # rclpy.shutdown()


# -functions---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def WuW(data):
    """transcribe the audio into texte and recognise wake up word or "stop"

    Args:
        data (table of bytes): audio that needs to be transcribe

    Returns:
        string: wake up word or "stop" or nothing
    """
    global temp_file
    global audio_model

    # Use AudioData to convert the raw data to wav data.
    last_sample = b''.join(data)

    # # Use AudioData to convert the raw data to wav data.
    audio_data = sr.AudioData(last_sample, SAMPLE_RATE, SAMPLE_WIDTH)
    wav_data = io.BytesIO(audio_data.get_wav_data())

    # Write wav data to the temporary file as bytes.
    with open(temp_file, 'w+b') as f:
        f.write(wav_data.read())

    # Read the transcription.
    result = audio_model.transcribe(temp_file, vad_filter=True)

    for segment in result[0]:
        line = segment.text

    try:
        # Removing punctuations using replace() method
        for punctuation in string.punctuation:
            line = line.replace(punctuation, '')
        # Try to find a wake up word
        if WAKEUPWORD in line.lower():
            WW = WAKEUPWORD
            return WW

        elif STOP in line.lower():
            WW = "stop"
            return WW
        else:
            return line.lower()
    except UnboundLocalError:
        pass


# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
