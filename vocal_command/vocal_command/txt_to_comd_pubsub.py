import configparser
import rclpy

from math import pi
from pathlib import Path
from rclpy.node import Node
from std_msgs.msg import String
from word2number import w2n

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -configuration variables-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

config = configparser.ConfigParser()

# Get right path
path_to_workspace_install = Path(__file__).parent.parent.parent.parent.parent.resolve()
path_to_config = path_to_workspace_install / "share" / "vocal_command" / "config" / "configuration.ini"

# Use new found path
config.read(str(path_to_config))

DEFAULT_SPEED = config["command.building"]["DefaultSpeed"]                                                 # Default speed before any modification
BIT_DISTANCE = config["command.building"]["BitDistance"]                                                   # Distance when we go a bit more
LOT_DISTANCE = config["command.building"]["LotDistance"]                                                   # Distance when we go a lot more

TIME_LAPSE = float(config["safety"]["TimeLapse"])

# -globale variables------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

command_list_vocab = {"go", "back", "turn", "left", "right", "backward", "further", "more", "for", "during"}    # Vocabulary used to find the right command
modification_list_vocab = {"change", "speed", "faster", "slower", "set"}                                        # vocabulary used to modify the command
command = ""                                                                                                    # The command that will be modify to move the robot
base = "{linear: {x:"                                                                                           # The start of the command used to move the robot
total_stop = "{linear: {x:0.0, y:0.0, z:0.0}, angular: {x:0.0, y:0.0, z:0.0}}"                                  # The command that make the robot stop

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -communication----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

class PubSubCmd(Node):
    """publish the command or key word to restart another node and subscribe to the result of the audio retranscription
    """
    def __init__(self):
        """initilazing the publisher and the subscriber
        """
        super().__init__('PubSub_cmd')
        self.publisher = self.create_publisher(String, '/vocal_command/exec', 10)
        self.publisher_sound = self.create_publisher(String, '/vocal_command/sound', 10)

        self.subscription = self.create_subscription(String, '/vocal_command/text', self.listener_callback, 10)
        self.subscription  # prevent unused variable warning

        self.publisher_safety = self.create_publisher(String, '/vocal_command/safety', 10)
        timer_period = TIME_LAPSE  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback_safety)

        self.publisher_stop = self.create_publisher(String, '/vocal_command/stop', 10)

    def timer_callback_safety(self):
        """action to do to notice the safety node that this node is functionning
        """
        msg = String()
        msg.data = "4"
        self.publisher_safety.publish(msg)

    def listener_callback(self, msg):
        """action to do when we get the audio retranscription

        Args:
            msg (string): the audio retranscription
        """
        self.get_logger().debug('I heard: "%s"' % msg.data)
        text = msg.data
        msg = String()
        if text != "":
            msg.data = main_text_to_comd(text, self)
            self.publisher.publish(msg)
            self.get_logger().debug('Publishing: "%s"' % msg.data)
            text = ""


def main():
    """main function to launch the publisher and subscriber
    """
    rclpy.init()
    publisher = PubSubCmd()
    rclpy.spin(publisher)

# -find debug--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def search_vocab(sentence, vocab):
    """look if we know at leat one word from the sentence

    Args:
        sentence (string): retranscription of what has been understood by the whisper model
        vocab (list of string): list of all the key words used to find the right command

    Returns:
        boolean: True if words from the vocab arg is in the sentence arg
    """

    word_list = sentence.split(' ')      # get the list of word in the sentence
    for i in word_list:
        if i in vocab:           # We found a word we know
            return True
    return False


def find_id_speed(com):
    """found the speed and its position in the command line

    Args:
        com (string): the command line

    Returns:
        int,string: the position and the speed value
    """
    speed = "0"                                     # default speed
    id = -1
    previous_j = ""                                 # keep previous character in memory
    for j in com:
        id = id + 1
        if j == "-":                                # if we find a negative speed (-x.y)
            speed = com[id:id+4]
            break
        if j.isdigit() and j != "0":                  # if we found a speed different from zero (x.y)
            if previous_j == ":":
                speed = com[id:id+3]
                break
            elif previous_j == ".":                 # if we found a speed different from zero (0.x)
                speed = com[id-2:id+1]
                id = id - 2
                break
        if j == "t":                               # the command is timed and we don't need to look at what's at the end of the command
            break
        previous_j = j

    return id, speed


def convertion(num):
    """convert os output into readable string. In our case it's a number.

    Args:
        num: os output
    Returns:
        string: the translation the os output
    """
    new_num = ""
    for i in str(num):
        if i == "n":                     # we found a number
            new_num = new_num + " "
        if ord(i) > 47 and ord(i) < 59:  # we make sure after conversion it is a number
            new_num = new_num + i
    return new_num

def find_number(txt):
    """find number from text or aleady number

    Args:
        txt (string): the text we want to find the number in

    Returns:
        float: the number we found
    """
    result = ""
    try:
        result = w2n.word_to_num(txt)
        return result
    except ValueError:
        for i in txt:
            if i.isdigit() or i == "." or i == ",":
                result += i
        if result != "":
            return float(result)
        else:
            return None


# -get partial command line-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

def wait(result, speed, node):
    """find the time we want to wait

    Args:
        result (string): result of the audio retranscription
        speed (string): the speed of the robot

    Returns:
        string: number of time we need to execute the command
    """
    time = 0

    if speed != 0:
        if " much" in result or " a lot" in result:
            time = LOT_DISTANCE / float(speed)                          # go for 50 cm
        elif " little" in result or " bit" in result:
            time = BIT_DISTANCE / float(speed)                          # go for 10 cm

        else:
            number = find_number(result)
            if number is not None:
                if " centimeter" in result:
                    distance = number / 100                                 # in m
                    time = distance / float(speed)                          # v=d/t in second
                elif " meter" in result:
                    distance = number                                       # en m
                    time = distance / float(speed)                          # v=d/t in second

                elif " second" in result:
                    time = number                                           # in seconds
                elif " minute" in result:
                    time = number*60                                        # in seconds

                elif "degree" in result:
                    time = number * (pi / 180) / float(speed)               # v=d/t in second with v converted from degree

    if time != 0:
        msg_sound = String()
        msg_sound.data = "understood"
        node.publisher_sound.publish(msg_sound)
        node.get_logger().info('Publishing: "%s" sound' % msg_sound.data)
        return str(time)
    else:
        node.get_logger().info("\033[0;91mERROR\033[0m")
        return None


def modify_speed(com, mult):
    """modify the speed

    Args:
        com (string): the command line
        mult (float): pourcentage by which we modify the speed

    Returns:
       string: the new command line
    """
    [id, speed] = find_id_speed(com)
    speed = float(speed) + float(speed) * mult
    comd = com[:id]
    comd = comd + str(speed) + total_stop[id+3::]       # Rebuild the new command line with the new speed
    return comd


def set_speed(com, mult):
    """set the speed to a specific value

    Args:
        com (string): the command line
        mult (float): pourcentage by which we modify the speed

    Returns:
       string: the new command line
    """

    id, speed = find_id_speed(com)
    if float(speed) < 0.0:
        comd = com[:id] + "-"                             # We add a minus before the speed since it's a negative speed and we want to robot to go in the same direction even if we change the speed value
    else:
        comd = com[:id]
    comd = comd + str(float(mult)) + total_stop[id+3::]   # Rebuild the new command line with the new speed
    return comd


# -get the command--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def build_command(result, last_command, node):
    """build the command to move the robot

    Args:
        result (string):retranscription of the audio
        last_command (string): last command executed

    Returns:
        string: command or "partially_understood"
    """
    global DEFAULT_SPEED
    command_timed = False
    command_build = False

    if " further" in result or " more" in result:
        speed = find_id_speed(last_command)[1]
        wait_time = wait(result, speed, node)
        if wait_time is not None:
            return last_command + " timed " + wait_time                                                                  # If the command is timed we add this key word to the command

    else:
        if "during" in result or " for " in result or " degree" in result or " further" in result or " more" in result:
            command_timed = True

        if "turn" in result:
            if "right" in result:
                command = base + "0.0, y:0.0, z:0.0}, angular: {x:0.0, y:0.0, z:"+"-" + DEFAULT_SPEED + "}}'"
                command_build = True
            elif "left" in result:
                command = base + "0.0, y:0.0, z:0.0}, angular: {x:0.0, y:0.0, z:" + DEFAULT_SPEED + "}}'"
                command_build = True

        elif "go" in result:
            if "back" in result or "backward" in result:
                command = base + "-" + DEFAULT_SPEED+", y:0.0, z:0.0}, angular: {x:0.0, y:0.0, z:0.0}}'"
                command_build = True
            elif "right" in result:
                command = base + "0.0, y:" + DEFAULT_SPEED+", z:0.0}, angular: {x:0.0, y:0.0, z:0.0}}'"
                command_build = True
            elif "left" in result or "lift" in result :
                command = base + "0.0, y:-" + DEFAULT_SPEED+", z:0.0}, angular: {x:0.0, y:0.0, z:0.0}}'"
                command_build = True
            elif " on" in result or "forward" in result:
                command = base + DEFAULT_SPEED + ", y:0.0, z:0.0}, angular: {x:0.0, y:0.0, z:0.0}}'"
                command_build = True

    if command_build:                                                                                                 # If the command is successfully build
        msg_sound = String()
        msg_sound.data = "understood"
        node.publisher_sound.publish(msg_sound)
        node.get_logger().debug('Publishing: "%s" sound' % msg_sound.data)
        if command_timed:
            node.get_logger().debug("command_timed")
            speed = find_id_speed(command)[1]
            wait_time = wait(result, speed, node)
            if wait_time is not None:
                return command + " timed " + wait_time
        else:
            return command

    else:
        node.get_logger().info("\033[0;91mCouldn't build the command\033[0m")
        return "partially_understood"                                                                                         # Returning key word "partially_understood" to let the node getting the vocal command that it should listen again


def build_command_modifier(result, actual_command, node):
    """building command while a first command is still beeing executed

    Args:
        result (string): retranscription of the audio
        command (string): command beeing executed

    Returns:
        string: command or "partially_understood"
    """
    new_command = ""

    if "faster" in result:
        new_command = modify_speed(actual_command, 0.5)
    elif "slower" in result:
        new_command = modify_speed(actual_command, -0.5)
    elif "change" in result or "set" in result:
        if "speed" in result:
            speed = find_number(result)                                                                             # Getting the number spelled in the audio retranscription result
            if speed is not None:
                new_command = set_speed(actual_command, speed)

    if new_command != "":                                                                                                   # the command is successfully build
        msg_sound = String()
        msg_sound.data = "understood"
        node.publisher_sound.publish(msg_sound)
        node.get_logger().debug('Publishing: "%s" sound' % msg_sound.data)
        return new_command
    else:
        node.get_logger().info("\033[0;91mCommand couldn't be modified\033[0m")
        return "partially_understood"                                                                                   # Returning key word "partially_understood" to let the node getting the vocal command that it should listen again


# -main-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def main_text_to_comd(txt, node):
    """main function changing text into command

    Args:
        txt (string): the text that needs to be change into a command

    Returns:
        string: the command
    """
    global times_not_understood                                                        # If we don't understand 2 consecutive time we need to start waiting for the wake up word again
    global command
    new_command = ""
    if "stop" in txt:
        msg_sound = String()
        msg_sound.data = "stop"
        node.publisher_sound.publish(msg_sound)
        node.get_logger().debug('Publishing: "%s" sound' % msg_sound.data)
        node.publisher_stop.publish(msg_sound)
        times_not_understood = 0

    elif search_vocab(txt, modification_list_vocab):
        new_command = build_command_modifier(txt, command, node)
        if new_command == "partially_understood":
            msg_sound = String()
            msg_sound.data = "partially_understood"
            node.publisher_sound.publish(msg_sound)
            node.get_logger().debug('Publishing: "%s" sound' % msg_sound.data)
        times_not_understood = 0

    elif search_vocab(txt, command_list_vocab):
        new_command = build_command(txt, command, node)
        if command == "partially_understood":
            msg_sound = String()
            msg_sound.data = "partially_understood"
            node.publisher_sound.publish(msg_sound)
            node.get_logger().debug('Publishing: "%s" sound' % msg_sound.data)
        times_not_understood = 0
    else:
        msg_sound = String()
        msg_sound.data = "not_understood"
        node.publisher_sound.publish(msg_sound)
        node.get_logger().debug('Publishing: "%s" sound' % msg_sound.data)
        new_command = "partially_understood"                                               # We didn't understand but it is only the first time so we need the node getting the vocal command to listen again
        try:
            times_not_understood = times_not_understood + 1
        except NameError:
            times_not_understood = 1

        if times_not_understood == 2:
            new_command = "not_understood"

    if new_command != "partially_understood" and new_command != "not_understood":
        command = new_command[:63]
    return new_command


# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
