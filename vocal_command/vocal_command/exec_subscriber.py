import configparser
import rclpy
from pathlib import Path
from geometry_msgs.msg import Twist
from rclpy.node import Node
from std_msgs.msg import String
from time import time, sleep


# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -configuration variables-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

config = configparser.ConfigParser()

# Get right path
path_to_workspace_install = Path(__file__).parent.parent.parent.parent.parent.resolve()
path_to_config = path_to_workspace_install / "share" / "vocal_command" / "config" / "configuration.ini"

# Use new found path
config.read(str(path_to_config))

WAKEUPWORD = config["wake.word"]["WakeUpWord"]

MAX_TIME = int(config["safety"]["MaxTime"])
TIME_LAPSE = float(config["safety"]["TimeLapse"])

# -Communication-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

class Pubsub_move(Node):
    """listen for commande that need to be executed
    """
    def __init__(self):
        """initiate the node
        """
        super().__init__('subscriber_exec')
        self.ww = ""
        self.com = ""
        self.stop = False

        self.subscription_exec = self.create_subscription(String, '/vocal_command/exec', self.listener_callback, 10)
        self.subscription_exec  # prevent unused variable warning

        self.subscription_wake = self.create_subscription(String, '/vocal_command/wakeup', self.listener_callback_wake, 10)
        self.subscription_wake  # prevent unused variable warning

        self.subscription_stop = self.create_subscription(String, '/vocal_command/stop', self.listener_callback_stop, 10)
        self.subscription_stop  # prevent unused variable warning

        self.publisher_move = self.create_publisher(Twist, '/mobile_base_controller/cmd_vel', 10)
        self.publisher_ok = self.create_publisher(String, '/vocal_command/exec', 10)

        self.publisher_safety = self.create_publisher(String, '/vocal_command/safety', 10)
        timer_period = TIME_LAPSE  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def listener_callback_stop(self, msg):
        self.get_logger().debug('I heard stop')
        self.stop = True

    def timer_callback(self):
        """action to do to notice the safety node that this node is functionning
        """
        msg = String()
        msg.data = "5"
        self.publisher_safety.publish(msg)

    def listener_callback_wake(self, msg):
        """action to do when hear a wake up word
        """
        self.get_logger().debug('I heard: "%s"' % msg.data)
        self.ww = msg.data

    def listener_callback(self, msg):
        """action the ndoe do when receiving a message fro 'exec' topic

        Args:
            msg (string): the command or key words that we will ignore here
        """
        self.get_logger().debug('I heard: "%s"' % msg.data)
        if msg.data != "partially_understood" and msg.data != "not_understood" and msg.data != "ok" and (self.ww == WAKEUPWORD or self.stop):
            self.com = msg.data
            msg_ok = String()
            msg_ok.data = "ok"
            self.publisher_ok.publish(msg_ok)
            self.get_logger().info('Publishing: "%s"' % msg_ok.data)




def main():
    """main function to launch the publisher and subscriber
    """
    rclpy.init()
    publisher = Pubsub_move()

    count = 0

    while True:
        if publisher.com != "":
            if publisher.com != "partially_understood" and publisher.com != "not_understood" and publisher.com != "ok" and (publisher.ww == WAKEUPWORD or publisher.stop):
                publisher.get_logger().debug('I heard: "%s"' % publisher.com)
                if publisher.stop:
                    msg_move = Twist()
                    msg_move.linear.x = 0.0
                    msg_move.linear.y = 0.0
                    msg_move.linear.z = 0.0
                    msg_move.angular.x = 0.0
                    msg_move.angular.y = 0.0
                    msg_move.angular.z = 0.0
                    publisher.publisher_move.publish(msg_move)
                    publisher.get_logger().debug('Publishing: "%s %s %s %s %s %s"' % (msg_move.linear.x, msg_move.linear.y, msg_move.linear.z, msg_move.angular.x, msg_move.angular.y, msg_move.angular.z))
                    count = 0
                    msg_ok = String()
                    msg_ok.data = "ok"
                    publisher.publisher_ok.publish(msg_ok)
                    publisher.get_logger().info('Publishing: "%s"' % msg_ok.data)
                    publisher.ww = ""
                    publisher.com = ""
                    publisher.stop = False

                else:
                    if count == 0:
                        t0 = time()
                        t02 = time()
                        t1 = time()
                    if "timed" in publisher.com:  # If the command needs to be executed for a certain time period
                        time_found = find_time(publisher.com)
                        publisher.get_logger().debug('time found: "%s"' % (time_found))
                    else:
                        time_found = MAX_TIME
                    if (t1 - t0 < time_found) or publisher.stop:
                        t2 = time()
                        if t2 - t02 >= TIME_LAPSE :
                            msg_safety = String()
                            msg_safety.data = "5"
                            publisher.publisher_safety.publish(msg_safety)
                            t02 = t2
                        speeds = find_id_speed(publisher.com)
                        msg_move = Twist()
                        msg_move.linear.x = float(speeds[0])
                        msg_move.linear.y = float(speeds[1])
                        msg_move.linear.z = float(speeds[2])
                        msg_move.angular.x = float(speeds[3])
                        msg_move.angular.y = float(speeds[4])
                        msg_move.angular.z = float(speeds[5])
                        publisher.publisher_move.publish(msg_move)
                        if count == 0:
                            publisher.get_logger().debug('Publishing: "%s %s %s %s %s %s"' % (msg_move.linear.x, msg_move.linear.y, msg_move.linear.z, msg_move.angular.x, msg_move.angular.y, msg_move.angular.z))
                            count += 1
                        t1 = time()
                        sleep(0.25)
                    else :
                        count = 0
                        publisher.ww = ""
                        publisher.com = ""
                        publisher.stop = False

        sleep(0.1)
        rclpy.spin_once(publisher)




# -commande----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def find_id_speed(command):
    """find the position and value of the speed in the command line

    Args:
        command (string): the command contening the speed for all the axes even rotation speed. it looks like: {linear: {x:0.0, y:0.0, z:0.0}, angular: {x:0.0, y:0.0, z:0.0}} with timed x optional

    Returns:
        table of string: list of all the speed found in the command
    """
    speed = []
    i = 0
    for i in range(len(command)):                                               # we go through every characters
        if command[i] == "x" or command[i] == "y" or command[i] == "z":
            if command[i+2] == "-":
                speed.append(command[i+2:i+6])
            else:
                speed.append(command[i+2:i+5])
    return speed                                                                # return the speed for each axes as a table


def find_time(command):
    """find the time when a command is  timed

    Args:
        command (string): the timed command

    Returns:
        float: the time
    """
    result = ""
    for i in reversed(command):
        print(i)
        if i != " ":
            result += i
        else:
            result = reversed(result)
            return float("".join(list(result)))


# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
