from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='vocal_command',
            executable='audio'
        ),
        Node(
            package='vocal_command',
            executable='ww'
        ),
        Node(
            package='vocal_command',
            executable='text'
        ),
        Node(
            package='vocal_command',
            executable='command'
        ),
        Node(
            package='vocal_command',
            executable='execution'
        ),
        Node(
            package='vocal_command',
            executable='sound'
        ),
        Node(
            package='vocal_command',
            executable='safety'
        ),
    ])
