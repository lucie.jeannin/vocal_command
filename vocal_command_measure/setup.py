import os

from glob import glob
from setuptools import setup

package_name = 'vocal_command_measure'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages', ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob('launch/vocal_command_measure_launch.py')),
        (os.path.join('share', package_name, 'config'), glob('config/*.ini')),
        (os.path.join('share', package_name, 'sound'), glob('sound/*.wav')),
        (os.path.join('share', package_name, 'test_for_measure'), glob('test_for_measure/*.wav')),
        (os.path.join('share', package_name, 'test_for_ww'), glob('test_for_ww/*.wav'))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='jeannin',
    maintainer_email='lucie.jeannin@inria.fr',
    description='command a robot by voice',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'audio = vocal_command_measure.voice_publisher:main',
            'ww = vocal_command_measure.ww_publisher:main',
            'text = vocal_command_measure.asr_publisher:main',
            'command = vocal_command_measure.txt_to_comd_pubsub:main',
            'execution = vocal_command_measure.exec_subscriber:main',
            'sound = vocal_command_measure.sound:main',
            'safety = vocal_command_measure.safety:main',
            'ww-test = vocal_command_measure.ww_test:main'
        ],
    },
)
