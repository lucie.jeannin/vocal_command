import configparser
import openpyxl as o
import os
import rclpy
import wave

from rclpy.node import Node
from std_msgs.msg import String
from time import time

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -configuration variables-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
config = configparser.ConfigParser()

# Get right path
path_to_file = os.path.abspath(os.path.dirname(__file__))[:-51] + "share/vocal_command_measure/config/configuration.ini"

# Use new found path
config.read(path_to_file)

TIME_LAPSE = float(config["safety"]["TimeLapse"])
WAKEUPWORD = config["wake.word"]["WakeUpWord"]

# -globale variables ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# name of the audio files and their contents
name = {1:"go on",2:"turn left",3 :"turn right",4 : "go back",5 : "go backward",6 : "go left", 7 : "go right",8 : "go for one second", 9 : "go during one second", 10 : "go for one minute", 11 : "go during one minute", 12 : "go for one centimeter", 13 : "go during one centimeter", 14 : "go for one meter", 15 : "go during one meter", 16 : "go faster", 17 : "go slower", 18 : "go ahead", 19 : "go in front of you", 20: "go a bit more", 21 : "go a bit further", 22 : "go a lot further", 23 : "go a lot more", 24 : "change the speed to five", 25 : "set the speed to five" }
name2 = {1:"go on",2:"turn left",3 :"turn right",4 : "go back",5 : "go backward",6 : "go left", 7 : "go right",8 : "go for 1 second", 9 : "go during 1 second", 10 : "go for 1 minute", 11 : "go during 1 minute", 12 : "go for 1 centimeter", 13 : "go during 1 centimeter", 14 : "go for 1 meter", 15 : "go during 1 meter", 16 : "go faster", 17 : "go slower", 18 : "go ahead", 19 : "go in front of you", 20: "go a bit more", 21 : "go a bit further", 22 : "go a lot further", 23 : "go a lot more", 24 : "change the speed to 5", 25 : "set the speed to 5" }

number_round = 9

# -measures----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def wer(ref, hyp ,debug=True):
    """calcul of the word error rate

    Args:
        ref (string): reference against which we will compare our text.
        hyp (string): the text we will compare to the reference
        debug (bool, optional): debug choice variable. Defaults to True.

    Returns:
        dictionnary: result of the WER and all the debug variables, in our case we only use de WER variables
    """
    r = ref.split()
    h = hyp.split()
    #costs will holds the costs, like in the Levenshtein distance algorithm
    costs = [[0 for inner in range(len(h)+1)] for outer in range(len(r)+1)]
    # backtrace will hold the operations we've done.
    # so we could later backtrace, like the WER algorithm requires us to.
    backtrace = [[0 for inner in range(len(h)+1)] for outer in range(len(r)+1)]

    OP_OK = 0
    OP_SUB = 1
    OP_INS = 2
    OP_DEL = 3
    DEL_PENALTY = 1
    INS_PENALTY = 1
    SUB_PENALTY = 1

    # First column represents the case where we achieve zero
    # hypothesis words by deleting all reference words.
    for i in range(1, len(r)+1):
        costs[i][0] = DEL_PENALTY*i
        backtrace[i][0] = OP_DEL

    # First row represents the case where we achieve the hypothesis
    # by inserting all hypothesis words into a zero-length reference.
    for j in range(1, len(h) + 1):
        costs[0][j] = INS_PENALTY * j
        backtrace[0][j] = OP_INS

    # computation
    for i in range(1, len(r)+1):
        for j in range(1, len(h)+1):
            if r[i-1] == h[j-1]:
                costs[i][j] = costs[i-1][j-1]
                backtrace[i][j] = OP_OK
            else:
                substitutionCost = costs[i-1][j-1] + SUB_PENALTY # penalty is always 1
                insertionCost    = costs[i][j-1] + INS_PENALTY   # penalty is always 1
                deletionCost     = costs[i-1][j] + DEL_PENALTY   # penalty is always 1

                costs[i][j] = min(substitutionCost, insertionCost, deletionCost)
                if costs[i][j] == substitutionCost:
                    backtrace[i][j] = OP_SUB
                elif costs[i][j] == insertionCost:
                    backtrace[i][j] = OP_INS
                else:
                    backtrace[i][j] = OP_DEL

    # back trace though the best route:
    i = len(r)
    j = len(h)
    numSub = 0
    numDel = 0
    numIns = 0
    numCor = 0
    if debug:
        #print("OP\tREF\tHYP")
        lines = []
    while i > 0 or j > 0:
        if backtrace[i][j] == OP_OK:
            numCor += 1
            i-=1
            j-=1
            if debug:
                lines.append("OK\t" + r[i]+"\t"+h[j])
        elif backtrace[i][j] == OP_SUB:
            numSub +=1
            i-=1
            j-=1
            if debug:
                lines.append("SUB\t" + r[i]+"\t"+h[j])
        elif backtrace[i][j] == OP_INS:
            numIns += 1
            j-=1
            if debug:
                lines.append("INS\t" + "****" + "\t" + h[j])
        elif backtrace[i][j] == OP_DEL:
            numDel += 1
            i-=1
            if debug:
                lines.append("DEL\t" + r[i]+"\t"+"****")
    if debug:
        lines = reversed(lines)
        #for line in lines:
            #print(line)
        #print("#cor " + str(numCor))
        #print("#sub " + str(numSub))
        #print("#del " + str(numDel))
        #print("#ins " + str(numIns))
    # return (numSub + numDel + numIns) / (float) (len(r))
    wer_result = round( (numSub + numDel + numIns) / (float) (len(r)), 3)
    return {'WER':wer_result, 'numCor':numCor, 'numSub':numSub, 'numIns':numIns, 'numDel':numDel, "numCount": len(r)}

#-communication-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

class Measure(Node):
    """node that measure the time of execution of all the other node and teh WER and RTF value.
    """

    def __init__(self):
        """initialiaze subscriber
        """
        super().__init__('measure')

        if len(list(name.keys())) == len(list(name2.keys())):
            self.number_file = len(list(name.keys()))
        else :
            self.get_logger().info("\033[0;91mone audio file missing in dictionnary\033[0m")
            exit()

        self.count = 2
        self.num = 1
        self.round = 1

        self.init_done = False

        try:
            # Try to load the existing workbook
            self.workbook = o.load_workbook('Measure.xlsx')
        except FileNotFoundError:
            # If the file doesn't exist, create a new workbook
            self.workbook = o.Workbook()
            # Save the new workbook to create the file
            self.workbook.save('Measure.xlsx')

        self.subscriber_safety = self.create_subscription(String, '/vocal_command/safety', self.listener_callback_safety, 10)
        self.subscriber_safety  # prevent unused variable warning

        self.publisher_audio = self.create_publisher(String, '/vocal_command/audio_buffer', 10)

        self.subscriber_ww = self.create_subscription(String, '/vocal_command/wakeup', self.listener_callback_ww, 10)
        self.subscriber_ww  # prevent unused variable warning

        self.subscriber_asr = self.create_subscription(String, '/vocal_command/text', self.listener_callback_asr, 10)
        self.subscriber_asr  # prevent unused variable warning

        self.subscriber_cmd = self.create_subscription(String, '/vocal_command/exec', self.listener_callback_cmd, 10)
        self.subscriber_cmd  # prevent unused variable warning

        self.publisher_safety = self.create_publisher(String, '/vocal_command/safety', 10)
        timer_period = TIME_LAPSE  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback_safety)

        self.publisher_exec = self.create_publisher(String, '/vocal_command/exec', 10)

    def timer_callback_safety(self):
        """action to do to notice the safety node that this node is functionning
        """
        msg = String()
        msg.data = "1"
        self.publisher_safety.publish(msg)

    def listener_callback_ww(self,ms):
        """action to do when hearing a wake up word

        Args:
            ms (string): wake up word
        """
        if ms.data == "stop" or ms.data == WAKEUPWORD:
            # Write data to specific cells in the new sheet
            t = time()
            self.new_sheet["A"+str(self.count)] = t
            self.new_sheet["G"+str(self.count)] = 0
            self.count += 1
            # Publish an audio containing a command
            msg=String()
            msg.data = os.path.abspath(os.path.dirname(__file__))[:-51] + "share/vocal_command_measure/test_for_measure/" + str(self.num) + ".wav"
            self.num += 1
            self.publisher_audio.publish(msg)
            t = time()
            self.new_sheet["B"+str(self.count)] = t
            # Get the audio length
            with wave.open(msg.data) as mywav:
                audio_duration = mywav.getnframes() / mywav.getframerate()
            self.new_sheet["H"+str(self.count)] = audio_duration
            # Save the new workbook to create the file
            self.workbook.save('Measure.xlsx')
        else:# if the wake up word is not detected we calculate the WEr but still send an audio command
            # Write data to specific cells in the new sheet
            t = time()
            self.new_sheet["A"+str(self.count)] = t
            self.new_sheet["G"+str(self.count)] = wer(WAKEUPWORD,ms.data)["WER"]
            self.count += 1
            msg=String()
            msg.data = os.path.abspath(os.path.dirname(__file__))[:-51] + "share/vocal_command_measure/test_for_measure/" + str(self.num) + ".wav"
            self.num += 1
            self.publisher_audio.publish(msg)
            t = time()
            self.new_sheet["B"+str(self.count)] = t
            # get the audio length
            with wave.open(msg.data) as mywav:
                audio_duration = mywav.getnframes() / mywav.getframerate()
            self.new_sheet["H"+str(self.count)] = audio_duration
            # Save the new workbook to create the file
            self.workbook.save('Measure.xlsx')

    def listener_callback_asr(self,ms):
        """action to do when hearing an audio retranscription

        Args:
            ms (string): audio retranscription
        """
        # Write data to specific cells in the new sheet
        t = time()
        self.new_sheet["C"+str(self.count)] = t
        self.new_sheet["G"+str(self.count)] = min(wer(name[self.num-1],ms.data)["WER"],wer(name2[self.num-1],ms.data)["WER"])
        # Save the new workbook to create the file
        self.workbook.save('Measure.xlsx')

    def listener_callback_cmd(self,ms):
        """action to do when hearing a key words, when the audio is finished beeing processed

        Args:
            ms (string): key words for the processe success
        """
        if ms.data != "partially_understood" and ms.data != "not_understood" and ms.data != "ok":
            # Write data to specific cells in the new sheet
            t = time()
            self.new_sheet["D"+str(self.count)] = t
            # Save the new workbook to create the file
            self.workbook.save('Measure.xlsx')

        elif self.init_done:

            if ms.data=="ok":
                try:
                    # Write data to specific cells in the new sheet
                    t = time()-2  # we remove 2 seconds from this time because we slept 2 seconds in the execution nodes to let the wake up word node time to listen again
                    self.new_sheet["E"+str(self.count)] = t
                    # Save the new workbook to create the file
                    self.workbook.save('Measure.xlsx')
                except AttributeError:
                    pass

            if self.num == (self.number_file + 1) or self.num == 1:  # When all or none of the 25 audio file where processed
                self.round += 1
                self.get_logger().info("\033[0;91m"+str(self.round)+"\033[0m")
                self.num = 1
                self.count = 2
                # Create a new sheet
                self.new_sheet = self.workbook.create_sheet('measure'+str(len(self.workbook.sheetnames)),len(self.workbook.sheetnames))
                self.new_sheet["A1"] = "ww"
                self.new_sheet["B1"] = "audio"
                self.new_sheet["C1"] = "txt"
                self.new_sheet["D1"] = "cmd"
                self.new_sheet["E1"] = "exec"
                self.new_sheet["G1"] = "wer"
                self.new_sheet["H1"] = "audio_time"
                # Save the new workbook to create the file
                self.workbook.save('Measure.xlsx')

            if self.num != 1 :
                self.count += 1

            if self.round <= number_round:#  Doing more than 9 round will make the apps script google app crash because of too much information to process
                msg= String()
                msg.data = os.path.abspath(os.path.dirname(__file__))[:-51] + "share/vocal_command_measure/test_for_measure/hello.wav"
                # Write data to specific cells in the new sheet
                t = time()
                self.new_sheet["B"+str(self.count)] = t
                with wave.open(msg.data) as mywav:
                    audio_duration = mywav.getnframes() / mywav.getframerate()
                self.new_sheet["H"+str(self.count)] = audio_duration
                # Save the new workbook to create the file
                self.workbook.save('Measure.xlsx')
                self.get_logger().debug("\033[0;91mICI\033[0m")
                self.publisher_audio.publish(msg)
            else :
                # Save the changes
                self.workbook.save('Measure.xlsx')
                self.get_logger().info("\033[0;91mEND END END END END END END END END\033[0m")
                self.destroy_node()
                rclpy.shutdown()

    def listener_callback_safety(self,ms):
        if ms.data == "12345":
            self.init_done = True
            msg=String()
            msg.data='ok'
            self.publisher_exec.publish(msg)


def main():
    """main function to launch the publisher and subscriber
    """
    try :
        rclpy.init()
    except RuntimeError :
        pass

    sub = Measure()
    rclpy.spin(sub)

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()