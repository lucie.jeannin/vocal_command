from faster_whisper import WhisperModel
import openpyxl as o
import os
import string
import wave

from time import time

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# -globale variables ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

audio_model = WhisperModel("tiny.en")

audio = {0:"false",1:"hello you",2:"hey computer"}
possible_ww = list(audio.values())

alphabet = string.ascii_uppercase

# -mesures----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
def wer(ref, hyp ,debug=True):
    """calcul of the word error rate

    Args:
        ref (string): reference against which we will compare our text.
        hyp (string): the text we will compare to the reference
        debug (bool, optional): debug choice variable. Defaults to True.

    Returns:
        dictionnary: result of the WER and all the debug variables, in our case we only use de WER variables
    """
    r = ref.split()
    h = hyp.split()
    #costs will holds the costs, like in the Levenshtein distance algorithm
    costs = [[0 for inner in range(len(h)+1)] for outer in range(len(r)+1)]
    # backtrace will hold the operations we've done.
    # so we could later backtrace, like the WER algorithm requires us to.
    backtrace = [[0 for inner in range(len(h)+1)] for outer in range(len(r)+1)]

    OP_OK = 0
    OP_SUB = 1
    OP_INS = 2
    OP_DEL = 3
    DEL_PENALTY = 1
    INS_PENALTY = 1
    SUB_PENALTY = 1

    # First column represents the case where we achieve zero
    # hypothesis words by deleting all reference words.
    for i in range(1, len(r)+1):
        costs[i][0] = DEL_PENALTY*i
        backtrace[i][0] = OP_DEL

    # First row represents the case where we achieve the hypothesis
    # by inserting all hypothesis words into a zero-length reference.
    for j in range(1, len(h) + 1):
        costs[0][j] = INS_PENALTY * j
        backtrace[0][j] = OP_INS

    # computation
    for i in range(1, len(r)+1):
        for j in range(1, len(h)+1):
            if r[i-1] == h[j-1]:
                costs[i][j] = costs[i-1][j-1]
                backtrace[i][j] = OP_OK
            else:
                substitutionCost = costs[i-1][j-1] + SUB_PENALTY # penalty is always 1
                insertionCost    = costs[i][j-1] + INS_PENALTY   # penalty is always 1
                deletionCost     = costs[i-1][j] + DEL_PENALTY   # penalty is always 1

                costs[i][j] = min(substitutionCost, insertionCost, deletionCost)
                if costs[i][j] == substitutionCost:
                    backtrace[i][j] = OP_SUB
                elif costs[i][j] == insertionCost:
                    backtrace[i][j] = OP_INS
                else:
                    backtrace[i][j] = OP_DEL

    # back trace though the best route:
    i = len(r)
    j = len(h)
    numSub = 0
    numDel = 0
    numIns = 0
    numCor = 0
    if debug:
        #print("OP\tREF\tHYP")
        lines = []
    while i > 0 or j > 0:
        if backtrace[i][j] == OP_OK:
            numCor += 1
            i-=1
            j-=1
            if debug:
                lines.append("OK\t" + r[i]+"\t"+h[j])
        elif backtrace[i][j] == OP_SUB:
            numSub +=1
            i-=1
            j-=1
            if debug:
                lines.append("SUB\t" + r[i]+"\t"+h[j])
        elif backtrace[i][j] == OP_INS:
            numIns += 1
            j-=1
            if debug:
                lines.append("INS\t" + "****" + "\t" + h[j])
        elif backtrace[i][j] == OP_DEL:
            numDel += 1
            i-=1
            if debug:
                lines.append("DEL\t" + r[i]+"\t"+"****")
    if debug:
        lines = reversed(lines)
        #for line in lines:
            #print(line)
        #print("#cor " + str(numCor))
        #print("#sub " + str(numSub))
        #print("#del " + str(numDel))
        #print("#ins " + str(numIns))
    # return (numSub + numDel + numIns) / (float) (len(r))
    wer_result = round( (numSub + numDel + numIns) / (float) (len(r)), 3)
    return {'WER':wer_result, 'numCor':numCor, 'numSub':numSub, 'numIns':numIns, 'numDel':numDel, "numCount": len(r)}

def WuW(data,wakeupword):
    """transcribe the audio into texte and recognise wake up word or "stop"

    Args:
        data (table of bytes): audio that needs to be transcribe

    Returns:
        string: wake up word or "stop" or "nothing"
    """
    global audio_model

    # Read the transcription.
    result = audio_model.transcribe(data, vad_filter=True)

    for segment in result[0]:
        line = segment.text

    try:
        # Removing punctuations using replace() method
        for punctuation in string.punctuation:
            line = line.replace(punctuation, '')
        # Try to find a wake up word
        return line.lower()

    except UnboundLocalError:
        return "nothing"

def main():
    # get a excel workbook
    try:
        # Try to load the existing workbook
        workbook = o.load_workbook('ww_evaluation.xlsx')
    except FileNotFoundError:
        # If the file doesn't exist, create a new workbook
        workbook = o.Workbook()
        # Save the new workbook to create the file
        workbook.save('ww_evaluation.xlsx')

    new_sheet = workbook.create_sheet('mesure' + str(len(workbook.sheetnames)),len(workbook.sheetnames))

    for y in range(4,len(audio)+4):
        new_sheet[alphabet[y+y-1] + '2'] = audio[y-4]
        new_sheet[alphabet[y+y-1] + '3'] = 'wer'
        new_sheet[alphabet[y+y] + '3'] = 'rtf'
    workbook.save('ww_evaluation.xlsx')

    new_sheet['B2'] = 'positif'
    new_sheet['C2'] = 'negatif'
    new_sheet['D3'] = 'sould be positif'
    new_sheet['D4'] = 'sould be negatif'

    precision_table = [0,0,0,0]#pp,mp,pm,mm

    possible_wer = 1000

    # process the possible wake up word
    for i in range(1,5):
        new_sheet["G" + str(i+3)] = i

        for j in range(4,len(audio)+4):
            WAKEUPWORD = audio[j-4]
            path_to_file = os.path.abspath(os.path.dirname(__file__))[:-51] + "share/vocal_command_measure/test_for_ww/" + str(j-4) + ".wav"

            t = time()
            ww = WuW(path_to_file,WAKEUPWORD)
            t = time() - t

            Wer = wer(audio[j-4],ww)["WER"]

            new_sheet[alphabet[j+j-1] + str(i+3)] = Wer

            with wave.open(path_to_file) as mywav:
                    audio_duration = mywav.getnframes() / mywav.getframerate()

            new_sheet[alphabet[j+j] + str(i+3)] = t / audio_duration

            if j != 3:#should be positif
                if Wer == 0:#positif
                    precision_table[0] = precision_table[0] + 1
                else:#negatif
                    precision_table[2] = precision_table[2] + 1
            else:#should be negatif
                for p in possible_ww:
                    possible_wer = min(possible_wer,wer(p,ww)["WER"])

                if possible_wer == 0:#positif
                    precision_table[1] = precision_table[1] + 1
                else:#negatif
                    precision_table[3] = precision_table[3] + 1

            workbook.save('ww_evaluation.xlsx')

    new_sheet['B3'] = precision_table[0]
    new_sheet['C3'] = precision_table[1]
    new_sheet['B4'] = precision_table[3]
    new_sheet['C4'] = precision_table[2]
    workbook.save('ww_evaluation.xlsx')


# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
