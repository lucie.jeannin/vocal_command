from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='vocal_command_measure',
            executable='audio'
        ),
        Node(
            package='vocal_command_measure',
            executable='ww'
        ),
        Node(
            package='vocal_command_measure',
            executable='text'
        ),
        Node(
            package='vocal_command_measure',
            executable='command'
        ),
        Node(
            package='vocal_command_measure',
            executable='execution'
        ),
        Node(
            package='vocal_command_measure',
            executable='sound'
        ),
        Node(
            package='vocal_command_measure',
            executable='safety'
        )
    ])
