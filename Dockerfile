FROM ros:humble
ENV ROS_DISTRO humble
ENV ROS_PYTHON_VERSION 3
ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/Paris"


RUN apt update
RUN apt install -y --allow-unauthenticated python3-pip
RUN apt install -y --allow-unauthenticated python-is-python3
RUN apt install -y --allow-unauthenticated sox
RUN apt install -y --allow-unauthenticated python3-pyaudio
RUN apt install -y --allow-unauthenticated ffmpeg
RUN python -m pip install --upgrade pip

WORKDIR /root/
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY vocal_command vocal_command_ws/src
WORKDIR vocal_command_ws/src


WORKDIR /root/vocal_command_ws
RUN rosdep install -i --from-path src --rosdistro humble -y
RUN colcon build


RUN echo "source /root/vocal_command_ws/install/setup.bash" >> /root/.bashrc

ENTRYPOINT ["/ros_entrypoint.sh"]

CMD ["bash"]