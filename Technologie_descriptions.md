# Vocal Command technologies explications
## Table of Contents
1. [Audio distribution](#audio-distribution)
2. [Silence detection](#silence-detection)
3. [Wake up word](#wake-up-word)
4. [Audio retranscription](#audio-retranscription)
5. [Command building](#command-building)
6. [Command usage](#command-usage)

## Vocabulary

- Wake up word : the word you use to start your system.
    - example : "Hey google" or "Alexa"
- Transformers : deep learning architecture.
- ASR : automatique Speech Recognition (transcribing audio into text).

## Audio distribution

This part of the program will continuously capture voice activity and store it in a buffer identified by bytes until instructed to stop. This functionality is made possible by using the [Speech Recognition python library](https://github.com/Uberi/speech_recognition/tree/master).
To terminate this listening process, we will look for moments of silence. By detecting silence, we can determine the conclusion of a sentence or the end of a command.

## Silence detection

To identify such instances of silence, we use [PyDub](https://github.com/jiaaro/pydub/tree/master), a Python library. This library allows us to calculate the average volume of the audio and ascertain whether the volume reaches a "silence" threshold. Additionally, PyDub helps determine if this period of silence is sufficiently long to denote the end of a sentence rather than just the conclusion of a word.


## Wake up word

The concept of wake-up word technology is widely recognized and implemented by many individuals and organizations, such as [Howl](https://github.com/castorini/howl) or [Raven](https://github.com/rhasspy/rhasspy-wake-raven). However, in our case, we have made the decision to develop our own approach. Specifically, we employ [Whisper API ](https://github.com/openai/whisper)ASR system to achieve this objective. Our system operates by constantly listening but remaining unresponsive unless the audio trwhisper
anscription matches the desired wake-up word.

Advantages:

- Relying on existing technologies would have significantly extended the overall development time.
- Avoidance of potential license-related complications.

Disadvantages:

- The memory consumption associated with our implementation is comparatively higher than alternatives like Howl or Raven.

## Audio retranscription

To enhance the accuracy and reduce the transcription time, we leveraged [Faster Whisper](https://github.com/guillaumekln/faster-whisper). This modified version of the Whisper API incorporates the [Ctranslate2](https://github.com/OpenNMT/CTranslate2/) engine, which uses [Transformers](https://towardsdatascience.com/transformers-141e32e69591) models.

## Command building

To construct the command, we utilize the transcribed text from the audio and compare it against predetermined words. For instance, we check if the word "go" appears in the transcription. If it does, we further examine if the word "back" is also present. If both words are found, we understand that the person issuing the command intends for the robot to move backward, thereby determining the specific command to be executed.

Other technologies, such as [Rasa](https://rasa.com/), could have been employed in this scenario. Rasa offers more efficient means of determining user intent, even with unanticipated vocabulary.

Advantages:

- Building the command in this manner is easier and faster compared to implementing Rasa.
- The approach considers a wide enough range of vocabulary.

Disadvantages:

- The method lacks adaptability and may not handle complex scenarios as effectively as Rasa.

## Command usage

To execute the command, we extract the identified speed and transform it into a [Twist](http://docs.ros.org/en/noetic/api/geometry_msgs/html/msg/Twist.html) message conforming to the ROS2 standard. Subsequently, we publish this message to the designated [ROS2 topic](https://docs.ros.org/en/humble/Tutorials/Beginner-CLI-Tools/Understanding-ROS2-Topics/Understanding-ROS2-Topics.html) used to command the robot.